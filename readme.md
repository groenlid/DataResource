### Environment variables in use ###
* USEREALDB boolean (true|false): Use real database if this is true, fake data if it's false
* DBUser string: Database user to connect with
* DBPass string: Database password for the given user
* DB string: Database name
* DBPort int: Database connection port (eg: 1433)
* DBServer string: Database server (eg: xxx.database.windows.net)
* DBFile string: Path to where the fake datafile exists. This should only be used in development mode.
* NODE_ENV string (production|development)
* USE_SSR boolean (true|false): Enable server-side-rendering.

Create a start.sh file with the given

```sh
USEREALDB="true" \
DBUser="username" \
DBPass="password" \
DBServer="database-server" \
DB="database-name" \
DBPort="port" \
DBFile="path_to_data_file" \
NODE_ENV="development" \
USE_SSR="true" \
npm run startdev

```

![Screen 002](https://gitlab.com/groenlid/DataResource/raw/master/screens/002.png)

http://codepen.io/anon/pen/JYXYeB


### Start contributing ###

```sh

```

#### Global node modules ####
* nodemon: Tool for easy reloading of application on file-change.

### TODO ###
* Dumb and smart components
* Redo folder/file structure
* Maybe new redux state structure
* Click on resource in navigation panel should extend children

import * as constants from '../constants'

const API_URI = 'http://127.0.0.1:8080'

export function load() {
  return dispatch => {
    fetch(`${API_URI}/data`)
    .then(response => response.json())
    .then(response => JSON.parse(response))
    .then(json => dispatch({
      type:constants.FETCHED_RESOURCES,
      resources: json.resources,
    }))
    .catch((error) => console.log(error))
  }
}

export function save_resource_to_server(resource) {
  return dispatch => {
    fetch(`${API_URI}/resource`, {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(resource)
    })
    .then(response => response.json())
    //.then(response => JSON.parse(response))
    .then(newResource => dispatch({
      type:constants.UPDATED_SERVER_RESOURCE,
      resource: newResource,
    }))
  }
}

export function loadFromServer(stringifiedData) {
  var json = JSON.parse(stringifiedData)
  return {
    type: constants.FETCHED_RESOURCES,
    resources: json.resources,
  }
}

export function update_resource(resourceCode, changedResource){
  return {
    type: constants.UPDATE_RESOURCE,
    resourceCode,
    changedResource
  }
}

export function modify_searchquery(searchQuery) {
  return {
    type: constants.MODIFIED_SEARCHQUERY,
    search_query: searchQuery
  }
}

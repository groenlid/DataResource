import 'babel/polyfill';
import React from 'react'
import browserHistory from 'react-router/lib/browserHistory'
import Root from './root'
import ReactDOM from 'react-dom'
import routes from './routes'
import Router from 'react-router'
import createStore from './utils/redux'
import 'whatwg-fetch'


var createRouter = () => <Router history={ browserHistory } scrollBehavior='none' children={routes}/>

ReactDOM.render(<Root createRouter={createRouter} store={createStore(window.__INITIAL_STATE__)} />, document.getElementById('content'));

'use strict';

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { load, modify_searchquery } from '../actions/resources'
import { expand_resource } from '../actions/tree'
import { bindActionCreators } from 'redux'
import Tree from './tree/Tree'
import Radium, {Style} from 'radium'
import { Header, Navigation, NavigationHeader, NavigationSearch, Logo, Pending, Search } from './index'
import deepAssign from 'deep-assign'

@Radium
@connect(state => ({
  resources: state.resources,
  search: state.search,
  tree: state.tree
}), dispatch => (
  bindActionCreators({ load, modify_searchquery, expand_resource }, dispatch)
))
export default class Application extends Component {
  componentDidMount () {
     let { load, resources } = this.props
     if(!resources.items || Object.keys(resources.items).length === 0)
      load()
  }

  constructor () {
    super()
    this.state = { showSearchPanel: false }
  }

  static propTypes = {
    children: React.PropTypes.any,
    actions: React.PropTypes.object
  }

  toggleSearch () {
    this.setState({showSearchPanel: !this.state.showSearchPanel})
  }

  handleKeydown (event) {
    if (event.ctrlKey && (event.keyCode === 102 || event.keyCode === 70)) {
    // Block CTRL + F and CTRL + f events
      this.toggleSearch();
      event.preventDefault();
    }
  }

  componentDidMount () {
    window.addEventListener('keydown', this.handleKeydown.bind(this));
  }

  componentWillUnmount () {
    window.removeEventListener('keydown', this.handleKeydown.bind(this));
  }

  render () {
    const { dispatch, children, resources, search, modify_searchquery, params, expand_resource, tree } = this.props
    let { items, originalOfChangedItems } = resources
    let getOrphanedResourceCodes = (res) => {
      return Object.keys(res).filter((key) => res[key].parent === null);
    }

    let { showSearchPanel } = this.state
    let contentPanelStyles = Object.assign({}, contentPanel.styles, showSearchPanel ? contentPanel.searchPanelIsshown : contentPanel.searchPanelIsNotShown)
    let searchPanelStyles = deepAssign({}, searchPanel.styles, showSearchPanel ? searchPanel.shown : searchPanel.notShown)

    return (
      <div style={{padding:40, backgroundColor:'#32A4DC', boxSizing:'border-box'}}>
        <div style={{display:'flex', minHeight:'100%', boxShadow:'0 0 40px rgba(0,0,0,0.4)'}}>
          <Navigation style={{flexGrow:3, flexShrink:0, flexBasis:0 }}>
            <Logo>
              <span style={searchStyle} onClick={() => this.toggleSearch()}><i className="material-icons">search</i></span>
            </Logo>
            <Tree resources={ items } toRender={ getOrphanedResourceCodes(items) } expanded={ tree.expanded } onExpandToggleClick={ expand_resource } />
          </Navigation>
          {/*<Search style={searchPanelStyles} resources={ items } placeholder='write your query here..' search_query={ search.search_query } onSearchQueryChanged={(e) => modify_searchquery(e)}></Search>*/}
          <div style={contentPanelStyles}>{ children }</div>
        </div>
      </div>
    )
  }
}

const searchStyle = {
  float: 'right',
  ':hover': {
    cursor: 'pointer'
  }
}

const searchPanel = {
  styles: {
    panel: {
      transition:'flex-grow 500ms ease',
      boxSizing:'border-box',
      flexShrink:0,
      flexBasis:0
    }
  },
  notShown: {
    panel: {
      flexGrow: '.00001' // https://code.google.com/p/chromium/issues/detail?id=460510
    }
  },
  shown: {
    panel: {
      flexGrow:3
    }
  }
}

const contentPanel = {
  styles: {
    backgroundColor:'rgb(244, 244, 244)',
    borderLeft:0,
    borderRadius: '0 3px 3px 0',
    transition:'flex-grow 500ms ease',
    flexShrink:0,
    flexBasis:0
  },
  searchPanelIsshown: {
    flexGrow:3
  },
  searchPanelIsNotShown: {
    flexGrow:6
  }
}

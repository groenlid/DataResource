'use strict';

import React, { Component } from 'react'
import Radium from 'radium'
import { removeHtml, humanDate } from '../../utils/helpers'
import { Link } from 'react-router'

@Radium
export default class Attribute extends Component {

  render() {
    let { attribute, key, resourceCode } = this.props
    let { lastUpdated, value, attributeType} = attribute

    return (
      <tr key={key} style={_styles.row}>
        <td style={{width:1, whiteSpace:'nowrap'}}>
          <Link to={`/resource/${resourceCode}/attribute/edit/${attribute.id}`} style={{padding:20, textDecoration:'none', display:'block'}}>
            <span style={{textTransform:'lowercase', fontSize:'0.8em', padding:'5px 10px', borderRadius: 5, backgroundColor: getAttributeLabelColor(attributeType), color:'#fff'}}>{attributeType.toLowerCase()}</span>
          </Link>
        </td>
        <td>
          <Link to={`/resource/${resourceCode}/attribute/edit/${attribute.id}`} style={{padding:20, color:'#69748a', textDecoration:'none', display:'block'}}>
            { removeHtml(value) }
          </Link>
        </td>
        {/* <td style={{color:'#c7c6cc', textAlign:'right', width:1, whiteSpace:'nowrap', padding: 20}}>{ humanDate(lastUpdated) }</td> */}
        {/* <td style={{width:1}}><Link to={`/resource/${resourceCode}/attribute/edit/${attribute.id}`} style={{color:'#c7c6cc', textAlign:'right', width:1, whiteSpace:'nowrap', padding: 20}}><i className="material-icons">mode_edit</i></Link></td>*/ }
      </tr>
    )
  }
}
const _styles = {
  row: {
    borderTop:'1px solid #f1f5f8',
    ':hover': {
      background:'#f2f5f7',
      cursor: 'pointer'
    }
  }
}

const getAttributeLabelColor = (attributeType) => {
    switch(attributeType.toLowerCase()){
      case 'banner':
        return '#F1684C'
      case 'localized string':
        return '#30A953'
      case 'int':
        return '#32A4DC'
      case 'relationship':
        return '#FBC041'
      default:
        return '#6b738a'
    }
}

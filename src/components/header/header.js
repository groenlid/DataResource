import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class Header extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <div style={styles}>
          {this.props.children}
      </div>
    )
  }
};

let styles = {
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100%',
  height: 31,
  backgroundColor: '#000000',
  backgroundImage: 'linear-gradient(#262626, #000000)',
  color: '#ffffff',
  lineHeight: 31,
  zIndex: 9999
}

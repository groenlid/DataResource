import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'

/* DONT MOCK */
jest.dontMock('../button');
jest.dontMock('radium');

const Button = require('../button').default

describe('Button', () => {

  it('shows the text given to it as children', () => {
    const text = 'Test'
    let button = TestUtils.renderIntoDocument(<Button>{ text }</Button>)
    let buttonNode = ReactDOM.findDOMNode(button)
    expect(buttonNode.textContent).toEqual(text)
  })

  it('accepts onClick-function which is called when button is clicked', () => {
    let mockFunction = jest.genMockFunction();
    let button = TestUtils.renderIntoDocument(<Button onClick={ mockFunction }></Button>)
    let buttonNode = ReactDOM.findDOMNode(button)
    TestUtils.Simulate.click(buttonNode);
    expect(mockFunction).toBeCalled();
  })

})

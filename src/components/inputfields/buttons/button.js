import React from 'react'
import Radium from 'radium'

const Button = ({ style, onClick, children }) => (
  <button type='button' style={ Object.assign({}, _style, style) } onClick={ onClick }>
    { children }
  </button>
)

Button.propTypes = {
  style: React.PropTypes.object,
  onClick: React.PropTypes.func
}

export default Radium(Button)

const _style = {
  textTransform: 'uppercase',
  background: '#f0f5f8',
  verticalAlign: 'middle',
  borderRadius: 3,
  padding: '5px 10px',
  border: 0,
  ':hover': {
    background:'#6b738a',
    color:'#fff'
  }
}

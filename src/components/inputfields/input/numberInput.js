'use strict';

import React, { Component } from 'react'

export default class Input extends Component {

    constructor(){
      super()
      this.state = { focus: false }
    }

    static propTypes = {
      value: React.PropTypes.number,
      onChange: React.PropTypes.func,
      onFocus: React.PropTypes.func,
      onBlur: React.PropTypes.func,
      disabled: React.PropTypes.bool
    }

    shouldShowPlaceholder(value){
      value = value || ''
      return value.length === 0
    }

    onFocus(e){
      this.setState({focus:true})
      if(this.props.onFocus)
        this.props.onFocus(e)
    }

    onBlur(e){
      this.setState({focus:false})
      if(this.props.onBlur)
        this.props.onBlur(e)
    }

    getLabelStyle(placeholderIsShown, inputInInFocus){
      if(placeholderIsShown)
        return labelStylesWhenHidden

      if(!inputInInFocus)
        return labelStylesWhenShown

      return labelStylesOnFocus
    }

    onChange(e){
      let { value } = e.target
      let valueAsInt = parseInt(value)
      if(this.props.onChange)
        this.props.onChange(valueAsInt)
    }

    render() {
      let { onKeyDown, value, placeholder, onChange, disabled } = this.props
      let { focus } = this.state
      let showPlaceholder = this.shouldShowPlaceholder(value)

      return (
          <div style={wrapperStyles}>
            <input
              type='number'
              style={ focus ? contentEditableFocusStyles : contentEditableStyles }
              onChange={ (e) => this.onChange(e)}
              ref='search'
              onFocus= { (e) => this.onFocus(e) }
              onBlur= { (e) => this.onBlur(e) }
              onKeyDown={ (e) => onKeyDown && onKeyDown(e) }
              placeholder={ placeholder }
              disabled={ disabled }
              value={ value } />
              <label style={ this.getLabelStyle(showPlaceholder, focus) }>{ placeholder }</label>
          </div>
        )
  }
}


let wrapperStyles = {
  position: 'relative',
  width: '100%',
  padding: '25px 0'
}

let contentEditableStyles = {
  border: 0,
  display: 'block',
  width: '100%',
  outline: 0,
  resize: 'none',
  fontSize: '1.7em',//'2em', way too big for our way to long codes
  borderBottom: '1px solid #f1f5f8',
  background:'inherit'
}

let contentEditableFocusStyles = {
  ...contentEditableStyles,
  borderBottom: '1px solid #408EDE'
}

let placeholderStyle = {
  ...contentEditableStyles,
  color: '#bbbbbb'
}

let placeholderStyleFocus = {
  ...placeholderStyle,
  borderBottom: '1px solid #408EDE'
}

let labelStylesWhenHidden = {
  position: 'absolute',
  top: 0,
  transition: 'top 0.7s ease, opacity 0.7s ease',
  opacity: 0,
  fontWeight:'bold',
  fontSize: '1.2em'
}

let labelStylesWhenShown = {
  ...labelStylesWhenHidden,
  opacity: 1,
  top: 3
}

let labelStylesOnFocus = {
  ...labelStylesWhenHidden,
  ...labelStylesWhenShown,
  color: '#408EDE'
}

import { default as React, Component }  from 'react'
import Editor from 'react-medium-editor'

export default class RichTextArea extends Component {

  static propTypes = {
    value: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired
  }

  render() {
    return (
      <Editor
          text={this.props.value}
          style={Object.assign(styles.editor, this.props.styles)}
          onChange={this.props.onChange}
          options={{toolbar: {buttons: ['bold', 'italic', 'underline', 'orderedlist', 'unorderedlist', 'anchor','removeFormat']}}}
        />
    )
  }
};

let styles = {
  editor: {
    //fontFamily: 'Georgia, Cambria, "Times New Roman", Times,serif',
    //fontWeight: 400,
    fontSize: 21,
    lineHeight: '1.51',
    letterSpacing: '-0.003em',
    padding:20
  }
}

'use strict';

import React, { Component } from 'react'

export default class Select extends Component {

    constructor(){
      super()
      this.state = { focus: false }
    }

    static propTypes = {
      value: React.PropTypes.string,
      onChange: React.PropTypes.func,
      options: React.PropTypes.array,
      label: React.PropTypes.string
    }

    onFocus(e){
      this.setState({focus:true})
      if(this.props.onFocus)
        this.props.onFocus(e)
    }

    onBlur(e){
      this.setState({focus:false})
      if(this.props.onBlur)
        this.props.onBlur(e)
    }

    getLabelStyle(inputInInFocus){
      return(inputInInFocus) ?
        labelStylesOnFocus :
        labelStyles;

    }

    onChange(e){
      let {value} = e.target
      if(this.props.onChange)
        this.props.onChange(value)
    }

    render() {
      let { onKeyDown, value, label, onChange, options } = this.props
      let { focus } = this.state
      let renderOptions = (o, i) => (
          <option value={o} key={i}>
            {o}
          </option>
      )

      return (
          <div style={wrapperStyles}>
            <select
              style={ focus ? contentEditableFocusStyles : contentEditableStyles }
              onChange={ (e) => this.onChange(e)}
              onFocus= { (e) => this.onFocus(e) }
              onBlur= { (e) => this.onBlur(e) }
              onKeyDown={ (e) => onKeyDown && onKeyDown(e) }
              value={value}>

              { options.map(renderOptions) }

            </select>
            <label style={ this.getLabelStyle(focus) }>{ label }</label>
          </div>
        )
  }
}


let wrapperStyles = {
  position: 'relative',
  width: '100%',
  padding: '25px 0'
}


/*
TODO: Add this
-webkit-appearance: none;
-moz-appearance: none;
appearance: none;
*/

let contentEditableStyles = {
  border: 0,
  display: 'block',
  width: '100%',
  outline: 0,
  borderBottom: '1px solid #f1f5f8',
  appearance:'none',
  background:'#fff',
  fontSize: '1.7em'
}

let contentEditableFocusStyles = {
  ...contentEditableStyles,
  borderBottom: '1px solid #408EDE'
}

let placeholderStyle = {
  ...contentEditableStyles,
  color: '#bbbbbb'
}

let placeholderStyleFocus = {
  ...placeholderStyle,
  borderBottom: '1px solid #408EDE'
}

let labelStyles = {
  position: 'absolute',
  transition: 'top 0.7s ease, opacity 0.7s ease',
  fontSize: '1.2em',
  color: 'rgb(175, 174, 176)',
  opacity: 1,
  top: 3
}

let labelStylesOnFocus = {
  ...labelStyles,
  color: '#408EDE'
}

import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class Logo extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <div style={styles}>
        <h3 style={headerStyle}>
            digiex <span style={dataresourcestyle}>dataresource</span>
        </h3>
        { this.props.children }
      </div>
    )
  }
};

let styles = {
  margin:0,
  padding: 20,
  //width: 'calc(100% - 40px)',
}

let headerStyle = {
  textTransform: 'uppercase',
  letterSpacing: -0.5,
  display:'inline',
  margin: 0
}

let dataresourcestyle = {
  color: '#70a8cd'
}

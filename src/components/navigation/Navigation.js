import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class Navigation extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <div style={styles}>
          {this.props.children}
      </div>
    )
  }
};

let styles = {
  backgroundColor: '#2A2F39',
  color: '#ffffff',
  borderRadius: '3px 0 0 3px'
}

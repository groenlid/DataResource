import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class NavigationHeader extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <div style={styles}>
          {this.props.title}
      </div>
    )
  }
};

let styles = {
  backgroundColor: '#383D40',
  color: '#859199',
  padding: 20,
  margin: 20,
  textAlign: 'center',
  fontWeight: 'bold'
}

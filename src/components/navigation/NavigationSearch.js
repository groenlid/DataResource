import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class NavigationSearch extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <input placeholder="search" style={styles}/>
    )
  }
};

let styles = {
  backgroundColor: '#696c75',
  color: '#fff',
  padding: 20,
  margin: '0 20px',
  width: 'calc(100% - 40px)',
  boxSizing:'border-box',
  border:0,
  borderRadius:3
}

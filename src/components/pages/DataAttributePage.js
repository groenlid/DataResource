'use strict';

import React, { Component } from 'react'
import { Select } from '../index'
import { connect } from 'react-redux'
import { RichTextArea, Input, Button } from '../index'

@connect(state => ({
  resources: state.resources,
}))
export default class DataAttributePage extends Component {

    getAttribute(){
      let { resources, params } = this.props
      let { items } = resources
      let { code, id } = params
      let resource = items[code]

      return resource.attributes.find(a => a.id.toString() === id)
    }

    getAttributeTypes(){
      let { resources } = this.props
      let { attributeTypes } = resources
      return attributeTypes
    }

    render() {
      let attribute = this.getAttribute()
      let attributeTypes = this.getAttributeTypes()

      return (
          <div style={styles.common}>
            <div style={styles.panel}>
              <div style={styles.halfBlocks}>
                <Select options={attributeTypes} label={'Attribute type'} value={attribute.attributeType} />
              </div>
              <div style={styles.halfBlocks}>
                <Input value={attribute.subResource} placeholder={'SubResource'}/>
              </div>
            </div>
            <RichTextArea value={attribute.value} onChange={(t) => {console.log(t)}} styles={Object.assign({},styles.panel, { display: 'block' })}/>
            <div style={ Object.assign({},styles.panel, styles.cornerPanel) }>
              <Button style={ styles.undoButton }>Undo pending changes</Button>
              <Button style={ styles.saveButton }>Save pending changes</Button>
            </div>
          </div>
        )
  }
}

const styles = {
  common: {
    fontSize: '0.8em',
    padding: 20
  },
  panel: {
    backgroundColor: 'rgb(255, 255, 254)',
    padding: 20,
    boxShadow: '1px 1px 1px rgb(230, 230, 230)',
    marginBottom: 20,
    display: 'flex'
  },
  cornerPanel: {
    justifyContent: 'space-between'
  },
  halfBlocks: {
    flexGrow: 1
  },
  undoButton: {
    ':hover': {
      background: 'rgb(249, 134, 134)',
      color:'#fff'
    }
  },
  saveButton: {
    ':hover': {
      background: 'rgb(102, 174, 95)',
      color: '#fff'
    }
  }
}

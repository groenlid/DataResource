'use strict';

import React, { Component } from 'react'
import Search from '../search/Search'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { modify_searchquery, update_resource, save_resource_to_server } from '../../actions/resources'
import { HTMLInput, Button, Input, NumberInput, Attribute } from '../index'
import moment from 'moment'
import Radium from 'radium'

@Radium
@connect(state => ({
  resources: state.resources
}),dispatch => (
  bindActionCreators({ modify_searchquery, update_resource, save_resource_to_server }, dispatch)
))
export default class DataresourcePage extends Component {

  getResource(){
    let { resources, params } = this.props
    console.log(params);
    let { code } = params
    return resources.items[code]
  }

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  render() {

    let { resources, params, update_resource, modify_searchquery, save_resource_to_server } = this.props,
      { code } = params,
      { items, search_query, pendingResource } = resources,
      resource = pendingResource || items[code] || {},
      { attributes, children, parent } = resource,
      hasChildren = this.props.children !== null

    const btnStyle = {
      fontSize: '0.8em',
      fontWeight: 'normal',
    }

    const addAttributeStyle = {
      ...btnStyle,
      float: 'right',
      marginBottom: '0.5em'
    }

    const headerStyle = {
      overflowX: 'hidden',
      fontSize: '2em',
      marginTop:0
    }

    const renderAttributes = (attr) => (
      <div style={styles.attributesPanel}>
        <h3 style={{color:'#444756', marginBottom: '0.5em'}}>Attributes <Button style={ addAttributeStyle }>Add attribute</Button></h3>
        <table style={{width: '100%'}}>
          <tbody>
            {attr.map((a, i) => <Attribute attribute={a} resourceCode={code} key={i} />)}
          </tbody>
        </table>
      </div>
    )

    const renderResourceForm = () => (
      <div>
        <div style={{width:'60%', display:'inline-block'}}>
          <Input value={resource.title} onChange={ (e) => update_resource(code, { ...resource, ...{ title: e } }) } placeholder='Title'/>
        </div>
        <div style={{width:'40%', display:'inline-block'}}>
          <NumberInput value={resource.sortOrder} onChange={ (e) => update_resource(code, { ...resource, ...{ sortOrder: e } }) } placeholder='Sort Order'/>
        </div>
        <div>
          <Input value={resource.description} onChange={ (e) => update_resource(code, { ...resource, ...{ description: e } }) } placeholder='Description'/>
        </div>
      </div>
    )

    return (
      <div>
        <div style={styles.resourcePanel}>
          <h1 style={ headerStyle } title={ code }>{ code }</h1>
          {
            !hasChildren && renderResourceForm()
          }
        </div>

        <div>
          { this.props.children || renderAttributes(attributes) }
        </div>
        {/* attributeToEdit ? renderEditAttribute(attributeToEdit) : renderAttributes(attributes) */}

        {
          <div>
            <Button style={{float:'right', background:'#5DB8F1', color:'#fff'}} onClick={() => save_resource_to_server(resource)}>Save this shit!</Button>
          </div>
        }
        {/*JSON.stringify(attributes)*/}
      </div>)
  }
}

const styles = {
  resourcePanel: {
    fontSize:'0.9em',
    borderRadius: '0px 3px 0px 0px',
    borderBottom: '1px solid #E6E6E6',
    boxSizing: 'border-box',
    padding: '20px 20px 0px',
    backgroundColor: '#fff'
  },
  attributesPanel: {
    fontSize: '0.9em',
    backgroundColor: 'rgb(255, 255, 254)',
    margin: '20px',
    padding: '20px',
    boxShadow: '1px 1px 1px rgb(230, 230, 230)'
  }
}

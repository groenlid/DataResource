'use strict';

import React, { Component } from 'react'
import Search from '../search/Search'

export default class SearchPage extends Component {

  onSearchQueryChanged(query) {
    let { actions } = this.props
    actions.modify_searchquery(query)
  }

  render() {
    throw Error('This is not used anymore')
    let { resourceStore } = this.props,
      { resources, search_query } = resourceStore,
      mainResource = resources['DigiEx'],
      children = mainResource !== undefined ? mainResource.children : [],
      parent = mainResource !== undefined ? mainResource.parent : undefined

    return (<div>
      <Search resourceNames={ children } searchQuery={search_query} parent={parent} onSearchQueryChanged={ this.onSearchQueryChanged.bind(this) } />
    </div>)
  }
}

import React, { Component } from 'react'
import Radium from 'radium'

@Radium
export default class Paper extends Component {
  render () {
    Object.assign(styles, this.props.style);
    return (
      <div style={styles}>
          {this.props.children}
      </div>
    )
  }
};

let styles = {
  background:'#fff',
  color:'#222',
  borderTop:'.25rem solid #E91E63',
  boxShadow:'0 2px 4px #212121',
  padding:'2rem'
}

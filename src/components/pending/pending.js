import React, { Component } from 'react'
import Radium from 'radium'
import { Link } from 'react-router'

//<span style={notificationStyle}><i className="material-icons">notifications</i><span><sub>2</sub></span></span>

@Radium
export default class Pending extends Component {
  constructor(){
    super()
    this.state = { showPopup: false }
  }

  togglePopup(){
      this.setState({ showPopup: !this.state.showPopup })
  }

  render () {
    Object.assign(styles, this.props.style)
    let { showPopup } = this.state
    let { pendingResources } = this.props
    let pendingChanges = pendingResources.length

    let popup = showPopup && (<div>
      <div style={backdropStyle} onClick={ this.togglePopup.bind(this) }></div>
      <div style={popupStyle}>
        <div style={popupHeaderStyle}>
          { getChangeText(pendingChanges) }
        </div>
        <ul style={popupListStyle}>
          { pendingResources.map((resource, i) => (<li style={popupListItemStyle} key={i}><Link to={`/resource/${resource}`}>{ resource }</Link></li>)) }
        </ul>
      </div>
    </div>)

    let hasPendingChanges = pendingChanges > 0
    let iconStyles = showPopup ? Object.assign({}, notificationStyles, notificationActiveStyles) : notificationStyles

    return (
      <span style={styles}>
          { hasPendingChanges
            ? (<span><i className='material-icons' onClick={ this.togglePopup.bind(this) } style={iconStyles}>notifications</i><sub>{ pendingChanges }</sub></span>)
            : (<span><i className='material-icons'>notifications_none</i></span>)
          }
        { popup }
      </span>
    )
  }
};

let getChangeText = (pendingChanges) => pendingChanges === 1 ? `${pendingChanges} unsaved resource` : `${pendingChanges} unsaved resources`

const popupListItemStyle = {
  color:'#000',
  padding:10,
  ':hover': {
    background:'#efefef',
    cursor: 'pointer'
  }
}

const popupListStyle = {
  width: '100%',
	background: '#fff',
  margin:0,
  padding:0,
  listStyleType:'none',
  fontSize: '0.9em'
}

const popupHeaderStyle = {
  height: 50,
  lineHeight: '50px',
	width: '100%',
	background: '#F1684C',
	borderRadius: '3px 3px 0 0',
	textAlign:'center'
}

const popupStyle = {
  width: 300,
	position: 'absolute',
	left: -150,
	top: 40,
	zIndex: 1,
	borderRadius: 3,
	boxShadow: '0 0 40px rgba(0,0,0,0.4)'
}

const backdropStyle = {
    position: 'fixed',
    width: '100%',
    height: '100%',
    background: 'rgba(0,0,0,0.3)',
    top: 0,
    left: 0,
    zIndex: 1
}

const styles = {
  float:'right',
  marginRight:15,
  position: 'relative',
  zIndex: 1
}

const notificationStyles = {
  //color:'#859199',
  ':hover': {
    cursor: 'pointer'
  }
}


const notificationActiveStyles = {
  zIndex:2,
  position: 'relative'
}

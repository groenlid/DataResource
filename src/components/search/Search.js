'use strict';

import { default as Router, Link } from 'react-router'
import { default as React, Component }  from 'react'
import FuzzySearch from 'fuzzysearch'
import deepAssign from 'deep-assign'
import Radium from 'radium'

@Radium
export default class Search extends Component {

  static propTypes = {
    resources: React.PropTypes.object.isRequired,
    search_query: React.PropTypes.string.isRequired,
    onSearchQueryChanged: React.PropTypes.func.isRequired
  }

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props) {
    super(props)
    this.state = { focusedPos:0 }
  }

  setQuery() {
    let searchValue = this.refs.search.value
    this.props.onSearchQueryChanged(searchValue)
    this.setState({focusedPos:0})
  }

  goToResource(resource) {
    this.setState({focusedPos:0})
    this.props.onSearchQueryChanged('')
    this.context.router.transitionTo(`/resource/${resource.code}`)
  }

  goToFocusedResource() {
    let { focusedPos } = this.state,
      { resourceNames, search_query } = this.props,
      focusedResource =  this.matchingResources(resourceNames, search_query)[focusedPos]

    this.goToResource(focusedResource)
  }

  matchingResources(resourceList = [], query = '') {
    let searchValue = query.toLowerCase();

    return searchValue === '' ?
      [] :
      resourceList.filter(x => FuzzySearch(searchValue, x.code.toLowerCase()));
  }

  /**
  Move the focus between the resources.
  Delta is the position of the wanted focused resource from the current
  focused resourced.
  */
  moveFocusedResource(delta = 0){
    let {search_query, resourceNames} = this.props,
        {focusedPos} = this.state,
        matchingResources = this.matchingResources(resourceNames, search_query);

    if(matchingResources.length === 0 || (focusedPos === 0 && delta === -1) || (focusedPos === matchingResources.length - 1 && delta === 1)){
      return;
    }

    this.setState({focusedPos: focusedPos + delta});
  }

  onKeyDown(e){
    let { keyCode } = e,
      { search_query, parent } = this.props

    switch(keyCode){
      case 38: // Up
        this.moveFocusedResource(-1)
      break
      case 40: // Down
        this.moveFocusedResource(1)
      break
      case 37: // Left
      break
      case 39: // Right
      break
      case 13: // Enter
        this.goToFocusedResource()
      break
      case 8: // Backspace
        if(search_query.length === 0 && parent !== undefined)
          this.goToResource(parent)
      break;
    }
  }

  setFocus() {
    this.setState({hasFocus: true})
  }

  removeFocus() {
    this.setState({hasFocus: false})
  }

  createResourceList(resources = {}) {
    return Object.keys(resources).map(code => resources[code]);
  }

  render() {
    let { resources, search_query, style, placeholder } = this.props
    let { focusedPos, hasFocus } = this.state

    let resourceList = this.createResourceList(resources)

    let matchingResources = this.matchingResources(resourceList, search_query)
    let mergedStyles = deepAssign({}, _styles, style)

    let resultItem = (item, i) => (
      <div style={deepAssign({},mergedStyles.resultItem, i === focusedPos ? mergedStyles.resultItemActive : {})} key={i}>
        <h3 style={mergedStyles.resultItemHeader}>{item.code}</h3>
        <ul style={mergedStyles.resultItemReasons}>
          <li>
            Attribute value: So you want to be a pirate, eh? <span style={mergedStyles.highlighted}> You look more like a flooring</span> inspector.
          </li>
        </ul>
      </div>
    )

        return (
        <div style={mergedStyles.panel}>
          <div style={mergedStyles.inputWrapper}>
            <input
              type='text'
              style={mergedStyles.input}
              onChange={ () => this.setQuery()}
              ref='search'
              autoFocus
              placeholder={ placeholder }
              onFocus= { () => this.setFocus() }
              onBlur= { () => this.removeFocus() }
              value= { search_query }
              onKeyDown={ (e) => this.onKeyDown(e) } />

            { matchingResources.slice(0,4).map((item, i) => resultItem(item,i)) }

          </div>
        </div>
        )
  }
}

const _styles = {
  panel: {
    background:'#F9FAFB',
    borderLeft:0,
    borderRight:'1px solid #dbe1e6',
  },
  input: {
    width:'100%',
    padding:15,
    border:0,
    color:'rgb(200, 212, 231)',
    boxSizing:'border-box',
    background: 'transparent',
    fontSize:'1.6em'
  },
  inputWrapper: {
  },
  resultItem: {
    padding: 20,
    boxSizing: 'border-box',
    borderTop: '1px solid #fff',
    borderBottom: '1px solid #e5e8eb',
    overflow: 'hidden',
    color: '#576071',
    ':hover': {
      cursor: 'pointer',
      borderLeft: '2px solid red'
    }
  },
  resultItemActive: {
    borderLeft: '2px solid red'
  },
  resultItemHeader: {
    margin: 0,
    padding: 0,
    color: '#949cae',
    textTransform: 'uppercase',
    fontSize: '0.9em',
    fontWeight: 'normal'
  },
  resultItemReasons: {
    padding: 0,
    listStyleType: 'none',
    fontSize: '0.9em',
    margin: 0,
    whiteSpace: 'nowrap'
  },
  highlighted: {
    background: '#FDF574'
  }
}

import React from 'react'
import Radium from 'radium'
import { Link } from 'react-router'

/*
ul -> komponent
  - li Kode
  - li Kode
  - li
    - ul Komponent
      -li Kode
      -li Kode
*/

const codeIsExpanded = (code, expanded) => expanded.indexOf(code) > -1

const Tree = ({ resources, toRender, expanded, isChild, onExpandToggleClick }) => (
  <ul style={ isChild ? _styles.ul : _styles.firstUl }>
    { toRender.map((code, i) => renderItem(code, i, resources, toRender, expanded, codeIsExpanded(code, expanded), onExpandToggleClick)) }
  </ul>
)

const renderToggle = (key, code, resources, isExpanded, onExpandToggleClick) => (
  resources[code].children.length === 0 ? <span key={ `toggle_${key}` } style={ _styles.noToggle }></span> :
  <i style={_styles.toggleIcon} key={ `toggle_${key}` } className='material-icons' onClick={ () => onExpandToggleClick(code) }>
    { isExpanded ? 'expand_more' : 'chevron_right' }
  </i>
)

const renderItem = ( code, i, resources, toRender, expanded, isExpanded, onExpandToggleClick ) => (
  <li key={ i } style={ _styles.li }>
    { renderToggle(i, code, resources, isExpanded, onExpandToggleClick) }
    <Link style={ _styles.code } key={ i } to={ `/resource/${code}` } >{ code }</Link>
    { isExpanded && <Tree resources={ resources } expanded={ expanded } toRender={ resources[code].children } isChild={ true } onExpandToggleClick={ onExpandToggleClick } /> }
  </li>
)

Tree.propTypes = {
  resources: React.PropTypes.object.isRequired,
  toRender: React.PropTypes.array.isRequired,
  expanded: React.PropTypes.array,
  isChild: React.PropTypes.bool,
  onExpandToggleClick: React.PropTypes.func.isRequired
}

export default Radium(Tree)

const _styles = {
  firstUl: {
    borderLeft: '2px solid #FF0000',
    backgroundColor: '#20272D',
    padding: 20,
    listStyleType: 'none'
  },
  ul: {
    padding: '10px 0 10px 20px',
    margin: 0,
    listStyleType: 'none'
  },
  li: {
    color:'#859199',
    padding: '10px 0 10px 0',
    position: 'relative',
    overflowWrap:'break-word'
  },
  noToggle: {
    display:'inline-block',
    width: 25
  },
  toggleIcon: {
    width: 25,
    lineHeight:'1em',
    verticalAlign:'bottom',
    fontSize:'1.3em',
    display:'inline-block',
    ':hover': {
      cursor: 'pointer'
    }
  },
  code: {
    color: '#859199',
    textDecoration:'none'
  }
}

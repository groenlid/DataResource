import { FETCHED_RESOURCES, UPDATE_RESOURCE, UPDATE_ATTRIBUTE } from '../constants'

const initialStore = {
  items: {},
  pendingResource: null,
  pendingAttribute: null,
  attributeTypes: {}
}

/*
let resourceHasChangedSinceOriginal = (resourceCode, resource, originalOfChangedItems) => {
  let originalResource = originalOfChangedItems[resourceCode] || {}

  return originalResource.description !== resource.description ||
    originalResource.sortOrder !== resource.sortOrder ||
    originalResource.title !== resource.title;
}

let getUpdatedOriginalOfChangedItems = (resourceCode, oldResource, updatedResource, originalOfChangedItems) => {
  let resourceHasChangedSinceOrigin = resourceHasChangedSinceOriginal(resourceCode, updatedResource, originalOfChangedItems)
  let resourceExistInOriginalObject = originalOfChangedItems.hasOwnProperty(resourceCode)
  let newOriginalOfChangedItems = { ...originalOfChangedItems }
// Has resource changed from original?
// If yes. Add old resource to originalOfChangedItems if it's not there already
// if no. Remove resource from originalOfChangedItems if it's there.
  if(resourceHasChangedSinceOrigin && !resourceExistInOriginalObject){
    newOriginalOfChangedItems[resourceCode] = oldResource
  }
  if(!resourceHasChangedSinceOrigin && resourceExistInOriginalObject){
    delete newOriginalOfChangedItems[resourceCode]
  }
  return newOriginalOfChangedItems
}*/


export default function resources(state = initialStore, action) {
  // this function returns the new state when an action comes
  switch (action.type) {
    case FETCHED_RESOURCES:
      return {
        ...state,
        items: action.resources,
      }
    case UPDATE_RESOURCE:
      let { pendingItems } = state
      let { changedResource } = action

      return {
        ...state,
        pendingResource: changedResource
      }
    case UPDATE_ATTRIBUTE:
    let { pendingAttribute } = state
    let { changedAttribute, attributeId } = action

    return {
      ...state,
      pendingAttribute: changedAttribute
    }
    default:
      return state
  }
}

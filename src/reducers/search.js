import * as constants from '../constants';

const initialStore = {
  search_query: ''
}

export default function search(state = initialStore, action) {
  // this function returns the new state when an action comes
  switch (action.type) {
    case constants.MODIFIED_SEARCHQUERY:
      return {
        ...state,
        search_query:action.search_query
      }
    default:
      return state
  }
}

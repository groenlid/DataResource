import * as constants from '../constants';

const initialStore = {
  expanded: []
}

export default function tree(state = initialStore, action) {
  switch (action.type) {
    case constants.TREE_TOGGLE:
      let expanded = state.expanded
      return expanded.indexOf(action.resource) === -1 ?
        Object.assign({}, state, {
          expanded: state.expanded.concat([action.resource])
        }) :
        Object.assign({}, state, {
          expanded: state.expanded.filter(e => e !== action.resource)
        })
    default:
      return state
  }
}

'use strict';
import React, { PropTypes, Component } from 'react'
import { Provider } from 'react-redux'
import * as resourceActions from './actions/resources'
import Radium from 'radium'

@Radium
export default class Root extends Component {
  static propTypes = {
    createRouter: PropTypes.func.isRequired,
    store: PropTypes.object.isRequired
  }

  render () {
    const { createRouter, store } = this.props
    return (
      <Provider store={store} style={{height:'100%'}}>
        { createRouter() }
      </Provider>
    )
  }
}

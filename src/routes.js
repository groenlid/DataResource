import * as components from './components/index'
import React from 'react'
import Router, { Route, IndexRoute } from 'react-router'

let {
  Application,
  WelcomePage,
  DataresourcePage,
  NotFoundPage,
  DataAttributePage,
} = components

export default (
  <Route path='/' component={Application}>
    <IndexRoute component={WelcomePage} />
    <Route path='resource/:code' component={DataresourcePage}>
      <Route path='attribute/edit/:id' component={DataAttributePage} />
    </Route>
    <Route path="*" component={NotFoundPage} />
  </Route>
)

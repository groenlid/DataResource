import express from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { RouterContext, match } from 'react-router'
import { createLocation } from 'history';
import routes from './routes';
import Root from './root';
import createStore from './utils/redux';
import { connect, fetchData, saveResource } from './utils/db';
import favicon from 'serve-favicon';
import compression from 'compression';
import bodyParser from 'body-parser';

/* create express server */
let app = express();

connect((err) => {
  if(err)
    console.error('Could not connect to database', err);
  else{
    console.log('Connected to the database');
    let port = process.env.PORT || 8080;
    app.listen(port, () => console.log(`Listening on http://localhost:${port}`));
  }
});

app.use(compression());
app.use(express.static('dist'));
app.use('/jspm_packages', express.static('jspm_packages'));
app.use('/config.js', express.static('config.js'));
app.use('/src', express.static('src'));
app.use(express.static('dist/css'));

app.use(favicon(__dirname + '/../favicon.ico'));

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.get('/data', (req, res) => {
  fetchData((err, data) => {
    res.setHeader('Content-Type', 'application/json');
    res.json(data)
  })
});

//var chokidarEvEmitter = require('chokidar-socket-emitter');
//chokidarEvEmitter({port: 8090, path: 'src'});

let jsonParser = bodyParser.json();

app.put('/resource', jsonParser, (req, res) => {
  let resource = req.body;
  saveResource(resource, (err, savedResource) => {
    res.json(savedResource);
  });
});

const htmlTemplate = (production, inititalStoreState, renderedBody = '') => {
  let state = inititalStoreState ?
    `window.__INITIAL_STATE__ = ${JSON.stringify(inititalStoreState)};` :
    ``;

  let scriptLoader = production ?
    `<script src="/app.js"></script>` :
    `<script>
        System.trace = true
//      System.import('capaj/systemjs-hot-reloader').then(function(HotReloader){
          System.import('src/client');
//          new HotReloader.default('http://localhost:8090')  // chokidar-socket-emitter port
//        })
      </script>`;
  return `
    <!doctype html>
    <html>
        <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
          <title></title>
          <meta name="description" content="">
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link rel="stylesheet" href="/css/styles.css" type="text/css">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
          <script src="/jspm_packages/system.js"></script>
          <script src="/config.js"></script>
        </head>
        <body>
            <script type="text/javascript">
            ${state}
            </script>
            <div id="content">${renderedBody}</div>
            ${scriptLoader}
        </body>
    </html>
    `
};

/* a single request handler receives every server request
   and routes through react-router */
app.get('*', (req, res) => {
    /* create a router and give it our routes
       and the requested path */

    if(process.env.USE_SSR !== 'true'){
      return res.send(htmlTemplate(process.env.NODE_ENV === 'production'));
    }

    let location = createLocation(req.url);
    match({ routes, location }, (error, redirectLocation, renderProps) => {
      if (redirectLocation)
        res.redirect(301, redirectLocation.pathname + redirectLocation.search)
      else if (error)
        res.status(500).send(error.message)
      else if (renderProps == null)
        res.status(404).send('Not found')
      else{
        fetchData((err, data) => {

          if(err)
            console.error(err)

          let renderedBody = ''
          let inititalStoreState = {
            resources: data
          }
          let radiumConfig = {
            userAgent: req.headers['user-agent']
          }

          try {
            renderedBody = ReactDOMServer.renderToString(
              <Root store={createStore(inititalStoreState)} createRouter={() => <RouterContext {...renderProps}/>} radiumConfig={radiumConfig} />
            );
          }catch(error){
            console.error(error);
          }

          res.send(htmlTemplate(process.env.NODE_ENV === 'production', inititalStoreState, renderedBody));

        })
      }
    });
});

import React from 'react';

export default function fetchData( fn ) {
  return DecoratedComponent => class ConnectorDecorator extends React.Component {
    static DecoratedComponent = DecoratedComponent;

    static onEnter = store => {

        return (state, transition, callback) => {

            var promise = fn(store, state, callback/*jxtest*/) || Promise.resolve(true)
            promise.then( result => callback(),
                   err => callback(err) );

            console.log( 'decorator > onEnter' );
        }
    }

    render() {
      return <DecoratedComponent {...this.props} />
    }
  };
};

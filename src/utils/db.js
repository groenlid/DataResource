import fs from 'fs';
import sql from 'mssql';

const config = {
    user: process.env.DBUser,
    password: process.env.DBPass,
    server: process.env.DBServer,
    database: process.env.DB,
    port: process.env.DBPort,
    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
}

const isUsingRealDatabase = () => {
  return process.env.USEREALDB === "true";
}

const getDatabaseFilePath = () => {
  return process.env.DBFile || './resources.json';
}

let connect = (cb) => {
  if(isUsingRealDatabase())
    sql.connect(config, cb);
  else
    cb();
}

sql.on('error', function(err) {
    console.error('An error occured with the connection to the database.', err);
    process.exit(err); // Shutdown the process, so the application can restart.
});

let getIdToCodeDictionary = (resources) => {
  console.time('getIdToCodeDictionary');
  var dictionary = {};
  resources.forEach(resource => {
    dictionary[resource.Id] = resource.Code;
  });
  console.timeEnd('getIdToCodeDictionary');
  return dictionary;
}

let getIdToAttributeTypeDictionary = (attributeTypes) => {
  var dictionary = {};
  attributeTypes.forEach(attributeType => {
    dictionary[attributeType.Id] = attributeType.Type;
  });
  return dictionary;
}


let structureFetchedData = (resources, attributes, attributeTypes) => {
  console.time('structureFetchedData');
  let structuredData = {},
      idToCodeDictionary = getIdToCodeDictionary(resources),
      rootNode;

  resources.forEach(resource => {
    let relatedToCode = idToCodeDictionary[resource.RelatedToId];
    structuredData[resource.Code] = {
      ...{
        children: [],
        attributes: []
      },
      ...structuredData[resource.Code],
      ...{
        title: resource.Title,
        description: resource.Description,
        sortOrder: resource.SortOrder,
        lastUpdated: resource.TimeStamp.toString(),
        lastUpdatedBy: resource.CreatedBy,
        code: resource.Code,
        parent: relatedToCode
      }
    };
    if(relatedToCode === undefined){
      rootNode = resource.Code;
      return;
    }

    let relatedToCodeResource = structuredData[relatedToCode];
    if(relatedToCodeResource === undefined)
      structuredData[relatedToCode] = {
        children: [resource.Code]
      }
    else if(relatedToCodeResource.children === undefined){
      relatedToCodeResource.children = [resource.Code]
    }
    else
      relatedToCodeResource.children.push(resource.Code);
  });

  let attributeTypeDictionary = getIdToAttributeTypeDictionary(attributeTypes);
  attributes.forEach(attribute => {
    let resourceCode = idToCodeDictionary[attribute.ResourceId],
      resource = structuredData[resourceCode];

      resource.attributes.push({
        attributeType: attributeTypeDictionary[attribute.AttributeTypeId],
        value: attribute.Value,
        resource: resourceCode,
        subResource: idToCodeDictionary[attribute.SubResourceId],
        lastUpdated: attribute.TimeStamp.toString(),
        lastUpdatedBy: attribute.CreatedBy,
        id: attribute.Id
      })
  });

  console.timeEnd('structureFetchedData');
  return {
    items: removeDigiExNode(structuredData),
    pendingResource: null,
    pendingAttribute: null,
    attributeTypes: attributeTypes.map(at => at.Type)
  };
}

let removeDigiExNode = (structuredData) => {
  delete structuredData.DigiEx
  Object.keys(structuredData).forEach((key) => {
    var resource = structuredData[key]
    if(resource.parent === 'DigiEx')
     resource.parent = null
  })
  return structuredData
}

let cache = {};
//< 86.169ms
let shouldFetchFromCache = (cb) => {
  if(!cache.time){
    cb(false);
    return;
  }
  console.time('Check cache validity')
  let lastChanged = new sql.Request().query(`
    SELECT MAX(subQuery.latest) as latest
    FROM (
      SELECT MAX(TimeStamp) as latest FROM Resource
      UNION ALL
      SELECT MAX(TimeStamp) as latest FROM Attribute
    ) as subQuery
  `);
    lastChanged.then(data => {
      console.timeEnd('Check cache validity')
      let latest = data[0].latest;
      let cacheIsvalid = new Date(latest) < cache.time;
      cb(cacheIsvalid);
    });
};

let insertDataIntoCache = data => {
  cache.time = new Date();
  cache.value = data;
  console.log('inserted data into cache')
  return data;
}

let fetchData = (cb) => {
  shouldFetchFromCache((cacheIsvalid) => {
    if(cacheIsvalid){
      cb(undefined, cache.value);
    }
    else {
      _fetchData(cb);
    }
  });
}

let _fetchData = (cb) => {
  if(!process.env.USEREALDB)
  {
    fs.readFile(getDatabaseFilePath(), { encoding: 'utf8' }, (err, data) => {
      cb(err, JSON.parse(data));
    });
    return;
  }

  console.time('fetching all resources');
  console.time('fetching all attributes');
  console.time('fetching all attributeTypes');

// TODO: Do the joins the the database
  let requestResources = new sql.Request().query('select * from Resource');
  let requestAttributes = new sql.Request().query('select ResourceId, AttributeTypeId, Value, SubResourceId, TimeStamp, CreatedBy, Id from Attribute');
  let requestAttributeTypes = new sql.Request().query('select * from AttributeType');

  requestResources.then(() => console.timeEnd('fetching all resources'));
  requestAttributes.then(() => console.timeEnd('fetching all attributes'));
  requestAttributeTypes.then(() => console.timeEnd('fetching all attributeTypes'));

  Promise.all([requestResources, requestAttributes, requestAttributeTypes])
  .then(data => structureFetchedData(data[0], data[1], data[2]))
  .then(data => insertDataIntoCache(data))
  .then(data => cb(undefined, data))
  .catch(error => {
    cb(error);
    console.error(error);
  })
}

/*{ children: [],
  attributes:
   [ { attributeType: 'Banner',
       value: 'NO',
       lastUpdated: 'Fri Aug 23 2013 16:14:04 GMT+0200 (Romance Daylight Time)',
       lastUpdatedBy: 'A0610168' } ],
  title: 'Språkkode for bokmål i Norge',
  description: 'Språkkode for bokmål i Norge',
  sortOrder: null,
  lastUpdated: 'Thu Oct 04 2012 16:53:11 GMT+0200 (Romance Daylight Time)',
  lastUpdatedBy: 'sre',
  code: 'SpraakKode_Bokmaal_Norge',
  parent: 'SpraakKode' }
*/

const saveResource_updateResource = (resource, transaction) => {
  let request = new sql.Request(transaction);
  request.input('description', sql.VarChar, resource.description);
  request.input('code', sql.VarChar, resource.code);
  request.input('description', sql.VarChar, resource.description);
  request.input('sortOrder', sql.Int, resource.sortOrder);
  request.input('title', sql.VarChar, resource.title);
  return request.query(`UPDATE Resource
    SET Description=@description,
    SortOrder=@sortOrder,
    Title=@title
    WHERE Code=@code`);
}

const saveResource_getAttributes = (resource, transaction) => {
  let request = new sql.Request(transaction);
  request.input('code', sql.VarChar, resource.code);
  return request.query(`
    SELECT a.*, at.Type, r.Code as ResourceCode, rsub.Code as SubResourceCode
    FROM Attribute a
    INNER JOIN Resource r ON r.Id = a.ResourceId
    LEFT JOIN Resource rsub ON rSub.Id = a.SubResourceId
    LEFT JOIN AttributeType at ON at.Id = a.AttributeTypeId
    WHERE r.Code=@code`);
}

const saveResource_persistAttributeChanges = (givenResource, existingAttributes, transaction) => {
  //AttributeTypeId
  //ResourceId
  //SubResourceId
  let { Code } = givenResource;
  let promises = [];
  // Deleted and updated attributes
  for(let existingAttribute of existingAttributes){
    let givenAttribute = givenResource.attributes.find(a =>
      a.attributeType === existingAttribute.Type &&
      Code === a.ResourceCode &&
      existingAttribute.subResource === a.SubResourceCode);

    if(!givenAttribute)
      promises.push(saveResource_deleteAttribute(Code, existingAttribute, transaction));
    else
      promises.push(saveResource_updateAttribute(Code, givenAttribute, transaction));
  }

  // New attributes
  for(let givenAttribute of givenResource.attributes){
    let existingAttribute = existingAttributes.find(a =>
      a.Type === givenAttribute.attributeType &&
      Code === a.ResourceCode &&
      givenAttribute.subResource === a.SubResourceCode);

    if(!existingAttribute)
      promises.push(saveResource_createAttribute(Code, givenAttribute, transaction));
  }
  return Promise.all(promises);
}

const saveResource_deleteAttribute = (resourceId, attributeToDelete, transaction) => {

}

const saveResource_createAttribute = (resourceId, attributeToCreate, transaction) => {
  console.log('creating new attribute');
  let request = new sql.Request(transaction);
  request.input('code', sql.VarChar, resource.code);
  request.input('code', sql.VarChar, resource.code);
  request.input('code', sql.VarChar, resource.code);
  request.input('code', sql.VarChar, resource.code);
  request.input('code', sql.VarChar, resource.code);

  return request.query(`
    INSERT INTO Attribute 'AttributeTypeId', 'ResourceId', 'Value', 'SubResourceId', 'FromDate', 'ToDate'
    VALUES (@attributeTypeId, @resourceId, @value, @subResourceId, @fromDate, @toDate)`);
}
/*
Id INT IDENTITY,
  AttributeTypeId INT NOT NULL,
  ResourceId INT NOT NULL,
  Value NVARCHAR(MAX) NULL,
  SubResourceId INT NULL,
  FromDate DATETIME NOT NULL,
  ToDate DATETIME NULL,
  */
const saveResource_updateAttribute = (resourceCode, attribute, transaction) => {
  let request = new sql.Request(transaction);
  request.verbose = true;
  request.input('value', sql.VarChar, attribute.value);
  return request.query(`
    UPDATE Attribute
    SET Value=@value
    WHERE 1=2`)
  }

const saveResource = (givenResource, cb) => {
  let { code } = givenResource;

  if(!process.env.USEREALDB)
  {
    cb(null, givenResource);
    return;
  }

  let transaction = new sql.Transaction();
  transaction.begin()
  .then(() => saveResource_updateResource(givenResource, transaction))
  .then(() => saveResource_getAttributes(givenResource, transaction))
  .then(attributes => saveResource_persistAttributeChanges(givenResource, attributes, transaction))
  .then(() => transaction.commit())
  .then(() => cb(null, resource))
  .catch(error => cb(error, null)); // Update body
    // Update relations
    // Update attributes
    // Update history
    // Commit transaction
    // Call callback

  // Do stuff

  /*Promise.all([])
  .then(data => transaction.commit())
  .catch(error => {
    transaction.rollback()
    cb(error);
  })*/
}


export {
  connect,
  fetchData,
  saveResource
}

import sanitizeHtml from 'sanitize-html'

let pad = number => number < 10 ? '0' + number : number

let humanDate = date => {
  if(typeof date === 'string') date = new Date(date)
    return `
    ${pad(date.getUTCDate())}-${pad(date.getUTCMonth() + 1)}-${date.getUTCFullYear()}
    ${pad(date.getUTCHours())}:${pad(date.getUTCMinutes())}:${pad(date.getUTCSeconds())} UTC`
}

let removeHtml = html => sanitizeHtml(html, {allowedTags:[], allowedAttributes:[]})

export {
  humanDate,
  pad,
  removeHtml
}

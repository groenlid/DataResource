import { createStore, combineReducers, applyMiddleware } from 'redux'
import { loggerMiddleware, thunkMiddleware } from '../middleware/index'
import * as reducers from '../reducers/index'

export default function(initialState){
  const createStoreWithMiddleware = applyMiddleware(loggerMiddleware, thunkMiddleware)(createStore)
  const reducer = combineReducers(reducers);
  var store = initialState ? createStoreWithMiddleware(reducer, initialState) : createStoreWithMiddleware(reducer);

  // Hot reloading
  //module.hot.accept('../reducers', () => {
  //  const nextRootReducer = require('../reducers/index');
  //  store.replaceReducer(nextRootReducer);
  //});

  return store;
};

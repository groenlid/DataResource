'use strict';

describe('ResourceActionCreators', function() {
  var action;

  beforeEach(function() {
    action = require('actions/ResourceActionCreators.js');
  });

  it('should be defined', function() {
    expect(action).toBeDefined();
  });
});

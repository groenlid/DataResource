'use strict';

describe('DataresourceApp', function () {
  var React = require('react/addons');
  var DataresourceApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    DataresourceApp = require('components/DataresourceApp.js');
    component = React.createElement(DataresourceApp);
  });

  it('should create a new instance of DataresourceApp', function () {
    expect(component).toBeDefined();
  });
});

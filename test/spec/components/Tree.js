'use strict';

describe('Tree', function () {
  var React = require('react/addons');
  var Tree, component;

  beforeEach(function () {
    Tree = require('components/Tree.js');
    component = React.createElement(Tree);
  });

  it('should create a new instance of Tree', function () {
    expect(component).toBeDefined();
  });
});
